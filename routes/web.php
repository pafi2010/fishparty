<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::get('/run', function (Request $request)
{
    $params = $request->all();
    $ctrl = new App\Http\Controllers\ProcessController(
        $request->input('karp'),
        $request->input('osetr'),
        $request->input('schuka'),
        $request->input('nuts')
    );
    $ctrl->run();
});

Route::get('/', function ()
{
    abort(404);
});
