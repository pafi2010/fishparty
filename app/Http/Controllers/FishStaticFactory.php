<?php

namespace App\Http\Controllers;


final class FishStaticFactory
{
    /**
     * @var array
     */
    static $fishKinds = [
        'karp' => \App\Http\Controllers\KarpController::class,
        'osetr' => \App\Http\Controllers\OsetrController::class,
        'schuka' => \App\Http\Controllers\SchukaController::class
    ];

    /**
     * @param string $fishKind
     * @return mixed
     */

    public static function factory($fishKind)
    {
        if (array_key_exists($fishKind, self::$fishKinds)) {
            return new self::$fishKinds[$fishKind];
        }

        throw new \InvalidArgumentException('Fish kind must be karp/osetr/schuka');
    }
}