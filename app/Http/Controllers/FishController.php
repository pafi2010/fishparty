<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FishInterface;

abstract class FishController extends Controller implements FishInterface
{

    protected $speed;
    protected $satiety;
    protected $name;

    /**
     * Увеличивает сытость рыбы
     * @return bool
     */
    public function eatPenaut()
    {
        $this->satiety += 2;
        return true;
    }

    /**
     * Возвращает прибавку к скорости, если рыба голодна
     * @return float|int
     */
    protected function isHungry()
    {
        if ( $this->satiety < 5 ) {
           return $this->speed / $this->satiety;
        }
        return 0;
    }

    /**
     * Возвращает сытость рыбы
     * @return int
     */
    public function getSatiety()
    {
        return $this->satiety;
    }

    /**
     * Возвращает скорость рыбы
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed + $this->isHungry();
    }

    /**
     * Возвращает имя рыбы
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


}
