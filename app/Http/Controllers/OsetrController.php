<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FishController;

class OsetrController extends FishController
{
    protected $name = 'Банко-осетр';
    protected $speed = 6;
    protected $satiety = 3;
}
