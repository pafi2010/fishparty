<?php

namespace App\Http\Controllers;

interface FishInterface
{
    public function eatPenaut();

    public function getSatiety();

    public function getSpeed();

    public function getName();

}