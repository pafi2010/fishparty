<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FishStaticFactory;

class ProcessController extends Controller
{

    protected $nuts;
    protected $fishCollection;
    protected $messages = [];

    /**
     * ProcessController constructor.
     * @param null $karp
     * @param null $osetr
     * @param null $schuka
     * @param null $nuts
     */
    public function __construct($karp = null, $osetr = null, $schuka = null, $nuts = null)
    {
        $this->nuts = $nuts;
        $this->fishCollection = collect();
        $this->registerFish([
            'karp' => $karp,
            'osetr' => $osetr,
            'schuka' => $schuka
        ]);
    }

    /**
     * Создает коллекцию рыб
     * @param array $fishes
     * @return void
     */
    public function registerFish(array $fishes)
    {
        foreach ($fishes as $type => $col) {
            if ($col != null) {
                for ($i =0; $i < $col; $i++) {
                    $this->fishCollection->push(FishStaticFactory::factory($type));
                }
            }
        }
    }

    /**
     * Записывает текст в лог
     * @param $text
     * @return void
     */
    protected function log($text)
    {
        array_push($this->messages, $text);
    }

    /**
     * Выводит лог на экран
     * @return void
     */
    public function printLog()
    {
        echo '<ol>';
        collect($this->messages)->each(function ($item, $key) {
            echo '<li>'.$item;
        });
        echo '</ol>';
    }


    /**
     * Моделирует ситуацию в аквариуме
     */
    public function run()
    {
        $fishCollection = $this->fishCollection;
        $counter = 0;

        //start
        for ($i = 1; $i <= $this->nuts; $i++) {

            //step 1
            $counter++;
            $this->log('Появляется орешек (ход '.$counter.')');

            //step 2
            $counter++;
            $fishCollection = $fishCollection->filter(function ($value, $key) use ($counter) {
                if ( $value->getSatiety() < 10 ) {
                    $this->log($value->getName().' устремляется за орешком со скоростью '.$value->getSpeed().' (ход '.$counter.')');
                    return true;
                } else {
                    $this->log($value->getName().' достаточно сыт, чтобы больше не участвовать в гонке (ход '.$counter.')');
                }
            });

            if ( $fishCollection->isEmpty() ) {
                $this->log('Все рыбы сыты, конец.');
                $this->printLog();
                exit();
            }

            //step 3
            $counter++;
            $maxSpeed = $fishCollection->reduce(function ($carry, $item) {
                if ( $carry === null ) return $item;
                return $carry->getSpeed() > $item->getSpeed() ? $carry : $item;
            })->getSpeed();

            $finalists = $fishCollection->filter(function ($value) use ($maxSpeed){
                return $value->getSpeed() === $maxSpeed;
            });

            if ($finalists->count() > 1) {
                //Если несколько рыб достигли ореха одновременно, пусть удача улыбнется одной из них!
                $winner = $finalists[random_int(0, $finalists->count()-1)];
            } else {
                $winner = $finalists->first();
            }

            if (! $winner->eatPenaut() ) {
                for ($c = $counter; $counter < $c+5; $counter++) {
                    $this->log($winner->getName().' выжидает подходящий момент, чтобы съесть орешек. (ход '.$counter.')');
                }
            }
            $this->log($winner->getName().' съедает орешек. Сытость становится '.$winner->getSatiety().' (ход '.$counter.')');

            $fishCollection->filter(function($item, $key) use ($winner, $counter){
                if ($item !== $winner){
                    $this->log($item->getName().' не успевает (ход '.$counter.')');
                    return $item;
                }
            });

        }

        //finish
        if (--$i == $this->nuts) {
            $this->log('Орешков не поступило, конец.');
            $this->printLog();
            exit();
        }
    }
}
