<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FishController;

class KarpController extends FishController
{
    protected $name = 'Займо-карп';
    protected $speed = 7;
    protected $satiety = 8;

    /**
     * Увеличивает сытость рыбы
     * С вероятностью 30%
     * @return bool
     */
    public function eatPenaut()
    {
        $this->satiety += 2;
        $lazyDay = [true, true, false, true, true, false, true, true, false, true];
        return array_random($lazyDay);
    }
}
