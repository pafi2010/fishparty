<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AquariumModel extends Model
{
    protected $table = 'aquarium';
}
