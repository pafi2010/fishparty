<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FishModel extends Model
{
    protected $table = 'fish';
}
